---
title: "Hookfly"
weight: 10
---

### General Principle

Hookfly is a flight technique for 2 tees.
It uses the hook and is a lot easier with a wall of hookthrough between the 2 tees.
Parts usually force you to use hookfly instead of hammerfly by removing the ability to hammer other tees.

Initiating hookfly (with a wall of hookthrough):

- be each one one side of the wall
- one of the tees jumps up and hooks the other tee as high as possible
- continuous cycle:
  - the currently higher tee hooks the lower tee

Each hook will pull the lower tee significantly higher than the other tee was.
Thereby you will gain height each hook and are able to fly upwards indefinitely.

To initiate a hookfly mid-air, one tee has to cancels its fall by doublejumping and hook the other tee up.

{{% vid hookfly-wall %}}

#### Timing

Hooking the other tee the moment you are are just barely above him won't get the other tee very high.
Instead, wait a little longer until

- your tee has reached a decent height
- and the other tee is still well in your hook range

#### Without Hookthrough Wall

Without the wall you will have the problem of bumping into each other, which will break the rhythm.

To counteract the missing wall, steer away from the other tee whenever you are hooking.
You must move far enough to not collide, but have to stop before the other tee has to aim its hook in order to not mess up the aiming.

{{% vid hookfly-free %}}

### Horizontal Hookfly

For horizontal flight, the 2 tees will no longer have a side dedicated for each, but will instead revolve around each other.

To hookfly horizontally:

- stand apart from each other
- the tee that stands in the desired direction:
  - jumps up
  - hooks the other tee
- continuous cycle:
  - whenever you are on the top left relative to the other tee, hook him
  - move right shortly after each hook

{{% vid hookfly-horizontal %}}

### Hammerhitfly

To throw another tee, jump above it and hook the tee upwards and hammer it while it is next to you.
You have to dodge the other tee by moving to the side to not have it bump into you.

In a nutshell, hammerhitfly is a very precise chain of 2 tees taking turns to throw the other tee up.

- initiate it by having one of the 2 tees throw the other tee up
- loop it by having the thrown tee hook the other tee right before it gets hammered and using the hook to throw once again

Only the currently hooking tee moves.
Move to the side after the start of the hook to dodge the other tee on its way up.
Optionally move towards the other tee again if you are out of hammer range.

If you fail the hammering, simply continue the cycle, you should be able to keep the fly up.
