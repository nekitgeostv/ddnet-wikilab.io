---
title: "Grenade Launcher"
weight: 40
---

The Grenade Launcher is an automatic projectile weapon that is centered around fast and vertical movement.

It fires a projectile which flies in an arc, exploding either on impact with another tee (not yourself) or any solid block.
Any tee close to the explosion will get pushed away from it, allowing them to gain speed.

![Rocket arc](/images/rocket-arc.png)

| Quickinfo  |       |
| ---------- | ----- |
| Gameskin   | ![Grenade Launcher](/images/grenade.png) |
| Crosshair  | ![Grenade Launcher crosshair](/images/grenade-crosshair.png) |
| Fire delay | 500ms |

### Horizontal Movement

To gain horizontal movement speed:

- walk in the desired direction
- shoot rockets in the opposite direction in the ground

Each explosion will propel you forwards, you will keep the gained speed as long as you keep walking in the same direction.

{{% vid grenade-horizontal-speed-1 %}}

One grenade won't get you very far. You can combine many tricks to get much further, for example:

- shoot one grenade up first for two simultaneous explosions
- use multiple grenade shots to the side
- get speed with other tools before the first explosion (e.g. by using the hook)
- rocket jumping on the last grenade shot

{{% vid grenade-horizontal-speed-2 %}}

### Rocket Jumping

{{% vid rocket-down %}}

Shooting downwards without jumping can get you up to 4#08 = 4.25 tiles.

To rocket jump:

- aim downwards
- jump, shoot right after

The delay between the jump and the explosion must be timed well. 

- shoot too late and you won't get very high
- shoot too early and you will already be off the ground for the jump. That will then cause you to double jump, which cancels your momentum 

{{% vid rocket-jump %}}

#### Using Walls

Similar to getting horizontal movement by continuously shooting at the floor, you can boost yourself up a wall by shooting at it at an angle.
It is important to stay directly next to the wall in order to take advantage of the explosions.

- stand directly next to the wall
- rocketjump up
- aim downwards, in the direction of the wall (about 45°)
- continuously shoot, while staying next to the wall by moving in its direction

Once you run out of upwards momentum:

- continue shooting till you will begin falling
- doublejump, directly followed by another shot
- continuiously shoot again, while still staying next to the wall by moving in its direction

{{% vid rocket-wall-up %}}

On some parts you will need to switch to the opposite wall while on the way up.

- if the wall is close, you can switch to it without interrupting the rhythm.
- if the wall is further away, you can propell yourself to it using a rocket and then use the double jump to continue upwards.

{{% vid grenade-switch-wall-1 %}}

#### Double Rocket Jump

The double rocket jump (aka 'double rocket') is a more advanced version of the rocket jump.  
It works by having 2 simultaneous rocket explosions (instead of just one) below you right after your jump.  

- shoot on rocket straight up
- wait till it will explode below you
- do a regular rocket jump

It didn't work?

- If the two explosion didn't happen simultaneous, try to adjust your timing with the rocket jump.
- If you still didn't get very high or lost your double jump, try to jump a little earlier

{{% vid double-rocket %}}

This trick is hard to pull off consistently.

#### Triple Rocket Jump

Triple rocket jumping is much harder than the double rocket jump.

- start by doing a double rocket jump
- while falling down again, fire 2 rockets straight up (hold the firing key, the second rocket should be shot about when you land)
- if timed correctly, the 2 rockets will explode at the same time below you
- couple the 2 rockets with a regular rocket jump for the triple rocket jump

{{% vid triple-rocket %}}
Due to the precise timings, triple rocket jumps are insanely hard to pull off consistently.

### Advanced Behaviour

- a grenade projectile will explode if it remains in the air for too long [exact time?]
- a rocket projectile has no width, meaning it can technically pass through tight gaps between blocks. To get it to work on purpose, keep in mind that it is easier the closer you are to the gap
[shot video of rocket passing through gap]
- since the grenade launcher is an automatic weapon it can be held down to shoot indefinitely, also directly on unfreeze
- if you kill while a grenade projectile is in the air, the projectile will disappear
