#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

def velocy_ramp(value, start=550.0, range=2000.0, curvature=1.4):
    if value < start:
        return np.float32(1.0)
    return np.float32(1.0) / (curvature ** ((value-start)/range));

# vel = subtiles / tick
# values tested:
#1409 -> walking
#1410 -> standing (x=550, y=41)
#1411 -> standing (x=700)
def find_first(vel, start):
    i = start
    while velocy_x(np.float32(i)) > vel:
        print(i, velocy_x(np.float32(i)))
        i += 1
    return i

def velocy_x(vel):
    return vel*velocy_ramp(vel*np.float32(50))

def tiles_per_sec(vel):
    return vel * 50 / 32

def main():
    # TODO: test numbers
    print("internal speed, when stop moving", find_first(1/256, 550.0))
    plt.title('Speed')
    #x = np.linspace(550.0, 50000, 100)
    x = np.float32(np.linspace(0, 4000, 300))
    y = np.array([tiles_per_sec(velocy_x(el)) for el in x])
    #x = tiles_per_sec(x)
    #y = tiles_per_sec(y)
    plt.xlabel('internal speed in Subtile/Tick')
    plt.ylabel('speed with velocy ramp in Subtile/Tick')
    plt.plot(x, y)
    plt.show()

if __name__ == '__main__':
    main()

